import argparse
import socket
import threading


def serveClient(clientToResolveSocket,clientIpAddress,portNumber):
    clientRequest= clientToResolveSocket.recv(4096)
    print "[!] Received data from the client (%s:%d):%s" % (clientIpAddress,portNumber,clientRequest)
    clientToResolveSocket.send("I am a server response, my versiopn is 3.2")
    clientToResolveSocket.close()
    
def startServer(portNumber):
    server=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server.bind("0.0.0.0",portNumber)
    server.listen(10)
    print '[+] Listening locally on port %d ... '% portNumber

    while True:
        client,address=server.accept()
        print "[+] Conntected locally on port %s:%d ... "% (address[0],address[1])
        serverClientThread=threading.Thread(target=serveClient,argp=(client,address[0],address[1]))
        serverClientThread.start()

def main():
    parser =argparse.ArgumentParser('TCP Server')
    parser.add_argument('-p','--port',type=int,help="The port number",default=4444)
    args=parser.parse_args()

    portNumber=args.port

    startServer(portNumber)

if __name__ == '__main__':
    main()
        