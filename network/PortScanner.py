import argparse
import socket

def printBanner(connSock,tgtPort):
    try:
        if(tgtPort==80):
            connSock.send("GET HTTP/1.1 \r\n")
        else:
            connSock.send("\r\n")
        results=connSock.recv(4096)
        print "[+] Banner" +str(results)
    except:
        print '[-] Banner not available \n'

def connScan(tgtHost,tgtPort):
    try:
        client=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        client.connect((tgtHost,tgtPort))
        print "[+] %d tcp open" % tgtPort
        printBanner(client,tgtPort)
    except:
        print "[+] %d tcp closed" %tgtPort
    finally:
        client.close()

def portScan(tgtHost,tgtPorts):
    try:
        tgtIp=socket.gethostname(tgtHost)
    except:
        print "[+] Error: Unknown Host"
        exit(0)

    try:
        tgtName=socket.gethostbyaddr(tgtIp)
        print "[+] -- Scan result for : "+tgtName[0] + " ---- "
    except:
        print "[+] -- Scan result for : " +tgtIp + " --- "
    setdefaultTimeout(10)

    for tgtPort in tgtPorts:
        connScan(tgtHost,int(tgtPort))
def main():
    parser=argparse.ArgumentParser('Smart TCP Client Scanner')
    parser.add_argument("-a","-address",type=str, help="The target IP address")
    parser.add_argument("-p","--port",type=str,help="The port number")
    args=parser.parse_args()

    ipaddress=args.address
    portNumbers=args.port.split(",")
    portScan(ipaddress,portNumbers)

if __name__ == '__main__':
    main()
        